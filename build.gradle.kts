import org.gradle.script.lang.kotlin.extra
import org.gradle.script.lang.kotlin.kotlinModule
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "pl.edu.wszib"
version = "1.0-SNAPSHOT"

buildscript {
    var kotlin_version: String by extra
    kotlin_version = "1.2.10"
    var springBootVersion: String by extra
    springBootVersion = "1.5.9.RELEASE"

    repositories {
        mavenCentral()
    }

    dependencies {
        classpath(kotlinModule("gradle-plugin", kotlin_version))
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
        classpath("org.jetbrains.kotlin:kotlin-allopen:$kotlin_version")
    }

}

apply {
    plugin("kotlin")
    plugin("kotlin-spring")
    plugin("org.springframework.boot")
}

val kotlin_version: String by extra


repositories {
    mavenCentral()
}

dependencies {
    compile(kotlinModule("stdlib-jdk8", kotlin_version))
    compile("org.springframework.boot:spring-boot-starter-web")
//    compile("org.springframework.boot:spring-boot-starter-security")
    compile("com.auth0:java-jwt:3.2.0")
    compile("io.github.microutils:kotlin-logging:1.5.3")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

