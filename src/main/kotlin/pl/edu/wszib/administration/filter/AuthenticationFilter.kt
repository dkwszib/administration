package pl.edu.wszib.administration.filter

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.TokenExpiredException
import com.fasterxml.jackson.databind.ObjectMapper
import mu.KLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import pl.edu.wszib.administration.api.error.ErrorResponse
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Component
class AuthenticationFilter(@Value("\${jwt.secret}") private val secret: String,
                           @Value("\${jwt.issuer}") private val issuer: String) : OncePerRequestFilter() {
    companion object : KLogging()

    override fun doFilterInternal(request: HttpServletRequest?, response: HttpServletResponse?, filterChain: FilterChain?) {
        logger.info { "Filtering token from the request" }
        val tokenHeader = request?.getHeader("Authorization").orEmpty()
        if (tokenHeader.isBlank()) {
            respondWithStatus(response!!, "No Authorization header", HttpStatus.UNAUTHORIZED)
            return
        }
        val token = tokenHeader.replaceFirst("Bearer ", "")

        try {
            validateToken(token)
        } catch (e: TokenExpiredException) {
            logger.warn { "Token expired" }
            respondWithStatus(response!!, "Token has expired", HttpStatus.FORBIDDEN)
            return
        }

        filterChain!!.doFilter(request, response)
    }

    private fun validateToken(token: String) {
        val jwtVerifier = JWT.require(Algorithm.HMAC256(secret)).withIssuer(issuer).build()
        jwtVerifier.verify(token)
    }

    private fun respondWithStatus(response: HttpServletResponse, message: String, status: HttpStatus) {
        val errorResponse = ErrorResponse(Date().time, message, status.value(), status.reasonPhrase)

        response.status = status.value()
        response.contentType = MediaType.APPLICATION_JSON_VALUE
        response.writer.write(convertToTJson(errorResponse))
    }

    private fun convertToTJson(body: ErrorResponse): String? {
        val objectMapper = ObjectMapper()
        return objectMapper.writeValueAsString(body)
    }

}