package pl.edu.wszib.administration.api.error

import mu.KLogging
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import pl.edu.wszib.administration.exceptions.ForbiddenException
import java.util.*
import javax.servlet.http.HttpServletResponse

@ControllerAdvice
class GlobalErrorHandler : ResponseEntityExceptionHandler() {

    companion object : KLogging()

    @ExceptionHandler
    @ResponseStatus(HttpStatus.FORBIDDEN)
    fun handleForbidden(ex: ForbiddenException, response: HttpServletResponse): ErrorResponse {
        logger.warn { "Handling ForbiddenException" }
        val status = HttpStatus.FORBIDDEN
        return ErrorResponse(Date().time, ex.message as String, status.value(), status.reasonPhrase)
    }

}