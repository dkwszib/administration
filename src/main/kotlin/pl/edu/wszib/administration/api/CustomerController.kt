package pl.edu.wszib.administration.api

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import pl.edu.wszib.administration.service.CustomerService

@RestController
class CustomerController(private val customerService: CustomerService) {

    @GetMapping(value = ["/customers"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findAll(@RequestHeader("Authorization") authorizationHeader: String) = customerService.getClients(authorizationHeader)

}