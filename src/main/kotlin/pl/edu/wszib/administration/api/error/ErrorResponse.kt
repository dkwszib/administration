package pl.edu.wszib.administration.api.error


data class ErrorResponse(val timestamp: Long, val message: String, val status: Int, val error: String)