package pl.edu.wszib.administration.api

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import pl.edu.wszib.administration.service.UserService

@RestController
class UserController(private val userService: UserService) {

    @GetMapping(value = ["/users"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findAll(@RequestHeader("Authorization") authorizationHeader: String) = userService.getUsers(authorizationHeader)
}