package pl.edu.wszib.administration.enums


enum class UserRole {
    ADMIN, USER, CUSTOMER;
}