package pl.edu.wszib.administration.service

import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service
import pl.edu.wszib.administration.enums.UserRole
import pl.edu.wszib.administration.exceptions.ForbiddenException

/**
 * CustomerService accessible for ADMIN and USER.
 */
@Service
class CustomerService(@Value("classpath:customers.json") private val customers: Resource) : AbstractService() {

    /**
     * @return static list of clients
     */
    fun getClients(authorizationHeader: String): String {
        val role = extractRole(authorizationHeader)
        return when (extractRole(authorizationHeader)) {
            UserRole.ADMIN, UserRole.USER -> readFromResource(customers)
            else -> throw ForbiddenException("User with role $role is forbidden to get clients list")
        }
    }

}