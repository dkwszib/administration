package pl.edu.wszib.administration.service

import com.auth0.jwt.JWT
import org.springframework.core.io.Resource
import pl.edu.wszib.administration.enums.UserRole
import pl.edu.wszib.administration.exceptions.UnauthorizedException

abstract class AbstractService {
    protected fun readFromResource(resource: Resource) = String(resource.file.readBytes())

    protected fun extractRole(authorizationHeader: String): UserRole {
        val token = authorizationHeader.replaceFirst("Bearer ", "")
        val roleClaim = JWT.decode(token).getClaim("role").asString()
        if (roleClaim.isNullOrBlank()) {
            throw UnauthorizedException("No role claim found")
        }
        return UserRole.valueOf(roleClaim)
    }

}