package pl.edu.wszib.administration.service

import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service
import pl.edu.wszib.administration.enums.UserRole
import pl.edu.wszib.administration.exceptions.ForbiddenException

/**
 * UserService accessible for ADMIN.
 */
@Service
class UserService(@Value("classpath:users.json") private val users: Resource) : AbstractService() {

    /**
     * @return static list of Users
     */
    fun getUsers(authorizationHeader: String): String {
        val role = extractRole(authorizationHeader)
        return when (extractRole(authorizationHeader)) {
            UserRole.ADMIN -> readFromResource(users)
            else -> throw ForbiddenException("User with role $role is forbidden to get users list")
        }
    }
}