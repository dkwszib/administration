package pl.edu.wszib.administration.exceptions


class UnauthorizedException(msg: String) : RuntimeException(msg)