package pl.edu.wszib.administration.exceptions


class ForbiddenException(msg: String) : RuntimeException(msg)