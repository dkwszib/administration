package pl.edu.wszib.administration.configuration

import org.springframework.context.annotation.Configuration
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer

@Configuration
class RootConfig : PropertySourcesPlaceholderConfigurer()
