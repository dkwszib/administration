package pl.edu.wszib.administration.configuration

import mu.KLogging
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.edu.wszib.administration.filter.AuthenticationFilter

@Configuration
class FilterConfiguration(private val authenticationFilter: AuthenticationFilter) {

    companion object : KLogging()

    @Bean
    fun authenticationFilterRegistrationBean(): FilterRegistrationBean {
        logger.info { "Configuring AuthenticationFilter" }
        val filter = FilterRegistrationBean(authenticationFilter)
        filter.setName("authenticationFilter")
        return filter
    }
}